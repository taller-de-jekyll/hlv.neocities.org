---
layout: post
title: "HackLabVioleta 4eva"
permalink: /acerca/
---

## Bienvenides al sitio del Hacklab Violeta

Somos un grupo errático y fluido derramándose y derrapando en torno a **tecnologías libres** y **transfeminismos**. 

Nos juntamos cuando pinta (una o dos veces por mes, a veces cada tres meses o mes y medio) a hackearnos y liberarnos y hacer lo mismo y lo 
propio con nuestros dispositivos. 

Nos llamamos así porque nuestros encuentros acontencen en el **Centro Cultural Feminista Tierra Violeta** (Tacuarí 538, Ciudad de Buenos 
Aires, Argentina). Pintó que quedara así. Hacklab en tanto espacio de **hacktivistas** y violeta por el cc, que es violeta de feminismo. Todos 
los colores son bienvenidos: nos gustan el magenta, cian y rosa, y cd.. multicolor arcoiris y unicornio.

Algunas cosas que hacemos y podés venir a hacer:

* liberar dispositivos

* conocer y bajar mucho software libre

* estudiar, modificar y compartir mucho software libre ;p

* estudiar, romper o romper y estudiar y tratar de hacer andar mucho software y hardware libre

* compartir, probar y criticar estrategias y software para mantenernos libres y segures (seguridad o cuidados digitales)

* aprender y desaprender a programar y desprogramar

* hacer sitios web

* probar nuevos sabores y colores electrónicos y digitales

* tomar mate y/o birra

* conocernos entre todys 

* revolución anal contra el capital y analquía

