---
layout: post
title: Cuarto Encuentro
author: hacklab violeta as Taller de Jekyll
---

# CUARTO ENCUENTRO
    
    Proyectos a trabajar
    
    * sitio de hlv
    * biblioteca nomada (para hlv, para utopiar) hiv = hacklab interdimensional voluble
    * 
    
    
Estamos revisando jekyllthemes.org, para elegir un tema sencillo con el que podamos levantar hoy el sitio, y luego editar!!

* vemos los cuerpos de nuestro sitio
* subimos el sitio a neocities.org

* hablamos de diseño brutalista: https://brutalist-web.design/

## Seleccioncitas

Votamos con puntos al lado de cada a uno que preferimos y elegimos el que saca más

### Orientadas a texto

https://chesterhow.github.io/tale/ ...

// ganamos!! gracias aptra!

* <https://dyutibarma.github.io/monochrome/> .
* <https://heiswayi.github.io/textlog/> .
* <https://code.liquidthink.net/>
* <https://kxxvii.github.io/Kikofri/> ..
* <http://jekyllthemes.org/themes/darcli/> ..
* <https://brxyxncorp.github.io/ultra-minimalista/> .
* <http://webjeda.com/hagura/> ..
* <https://hemangsk.github.io/DevJournal/>
* <https://victorvoid.me/space-jekyll-template/>

### Orientadas a imágenes

* <http://jekyllthemes.org/themes/arcana/> ...
* <http://jekyllthemes.org/themes/millennial/>
* <http://webjeda.com/bheema/> .


## ejemplos de sitios cuya estetica brutalista esta en nuestros corazones

https://neocities.org/browse

# Pasos a seguir una vez que seleccionamos el tema o plantilla o theme

Elegimos el tema: https://chesterhow.github.io/tale/
Entramos en: https://github.com/chesterhow/tale/  que es el source en github

## la estructura del sitio

Las carpetas que empiezan con _ suelen ser especiales de Jekyll.
 La primera carpeta _includes contiene los elementos reutilizables de una plantilla, se denominan "partials". 
 la includes es opcional, a diferencia de _layouts
 La carpeta _layouts contiene la plantilla entera
 _post es para meter todos los articulos
 La carpeta _sass contiene los archivos escritos con el preprocesador Sass, que nos permite usar variables, etc., para no tener que mantener un archivo CSS gigante. Sass compila estos archivos a CSS para uso en el navegador.
 En assets van los recursos que vamos a usar, es decir los archivos
 en .gitignore van los archivos que no queremos tener adentro de git, como _site
 tiene un codigo de conducta! ah! esta en md
 // recordamos que en nuestras traducciones queremos corregir para que el lenguaje no sea excluyente
 Gemfile es un archivo especial que usa bundle que es el manejador de dependencias de ruby. Y lo que se pone ahi es de que gemas depende ese proyecto.
 tale.gemspec      es un archivo que no es estrictamente necesario para jekyll que define metadatos de una gema. (una gema es un paquete de ruby, el lenguaje de programación) el gemfile tiene una opcion para mandar al gemspec
 LICENSE , el licenciamiento de nuestro sitio
 README, es el archivo de entrada al proyecto (lo podemos encontrar muchas veces con la extensión .md) 
 El archivo _config.yml define la configuración de Jekyll escrito en YAML, otro lenguaje de marcado en texto plano. Lo que definamos acá después podemos usarlo en la plantilla.
about.md - empieza con el frontmatter (el cuadrito, o bloque, que se ve en raw como si estuviera entre los 3 guiones) (los metadatos del md, en yaml): mejor verlo en formato ro (raw).ej; layout, apunta a post. si vas a la carpeta layout, lo encontras dentro del contenido.... lo seguimos viendo igual, para prestar mas atención a los aspectos de la estructura
favicon: iconito entre las pestanias (.ico). se podrian hacer en png, pero se perdería la compatibilidad con viejos explorers
index.html es la entrada a un sitio/ directorio. Es el archivo que carga el navegador. Que sea, html quiere decir que ese directorio se puede navegar via web.

## consideraciones para la instalación

en las instrucciones del tema que elegimos, vemos que nos manda a instalar como gema de ruby. la idea es que cada plantilla venga como paquete, y que puedas instalarlo y salir andando. pero... eso vuelve opaca a la plantilla.
optamos por un metodo "anterior", descripto abajo: fork
por que? porque nos da acceso a todo el desarrollo y su historia. el proceso es mas transparente.
como lo hacemos? lo podemos probar todas, pero lo va a subir al repo de gitlab solo una. las demas lo forkean // esto seria en el caso de que subieramos el proyecto de cero.
es nuestro caso? no! porque tomamos el repo que ya existe en github, es decir, lo tomamos de alli (ya vemos su historia)

## pasos 

1. clonamos el repo
(acá dejamos una hoja de referencia de git donde podemos ver cuales son los comandos de git y para qué sirven: https://services.github.com/on-demand/downloads/es_ES/github-git-cheat-sheet.pdf  )

* abrir terminal 
* clonar :  copiar la dirección del repo: para eso escribimos en la terminal git clone https://github.com/chesterhow/tale/
lo que estamos haciendo es traernos el repositorio a nuestra máquina. es decir, va a generar un directorio (con todo lo que venga) con el mismo nombre == tale
// si dejamos el nombre del directorio tal cual como el nombre del tema, nos puede ayudar a no olvidarnos como se llamaba cuando lo modifiquemos :p
// pero queremos que la carpeta se llame como nuestro sitio en este caso. le ponemos hlv.neocities.org
ergo, clonamos de esta manera para hacerlo en un paso:
    ```git clone https://github.com/chesterhow/tale/ hlv.neocities.org```
* nos situamos alli: cd al directorio que acabamos de clonar. 

~~~

cd hlv.neocities.org

~~~
* a continuacion, instalaríamos dentro de nuestra carpeta, donde ya estamos, las gemas. como somos precavidas, queremos saber que version de ruby tenemos instalada antes

ruby -v
rbenv --version

// una cosa que puede y suele pasar cuando corremos estos comandos, es que no encuentre las versiones (te tira un not found).
Si cuando hacemos ruby -v nos muestra una versión distinta a la 2.5.1 lo que tenemos que hacer es buscar en nuestra carpeta personal dos archivos que se llamen .bash_profile  y .bashrc , copiamos el contenido de .bash_profile y lo pegamos al final de .bashrc
//rbenv instala distintas versiones de ruby por fuera de los repositorios. 
// deprecar == obsoletar

También puede pasar que te diga que no está instalado ruby y que podes instalarlo. Es necesario actualizar la info del bash, escribir source ~/.bash_profile en la terminal, no hace nada pero cuando volves a pedir la versión de ruby, ya te la muestra.

--Problemas con g++ en Fedora -> el paquete se llama gcc-c++

como hacemo por terminal? vamos a nuestro home, y escribimos 

``` cat .bash_profile >> .bashrc ```

luego con un editor abrimos el fichero .bashrc y editamos: sacamos la linea que dice 

``` [[ -f ~/.bashrc ]] && . ~/.bashrc ```

esa linea lo que hace es mandar a que lea directamente desde ese archivo, nos estamos pasandole el contenido de .bash_profile, para que pueda leer "indistintamente". despues completamos la explicacion :p



// importante!! queremos checar que tengamos las mismas versiones para eliminar la max posibilidad de conflictos: 2.5.1
// como segun el momento de desarrollo podes tener distintas versiones, el bundler te saca cuales son las versiones gemas o de dependencias asociadas a un proyecto. instalamos bundle
// reflexion. se llama bundler. cuando instalamos, hacemos bundle :p

* bundle install 
// aca vamos a hacerlo de esta manera, indicando el directorio de guardado, para que unifique el lugar donde guarda las gemas. esto lo vuelve mas dinamico y hace que se ahorre espacio.

``` bundle install --path=~/.gem ```

* bundle binstub jekyll

``` bundle binstub jekyll```

// binstub insala los bins de una gema, crea un directorio bin y adentro de eso, jekyll
``` ./bin/jekyll ```
// corre, ejecuta el jekyll dentro de ese directorio, relativo a este proyecto
// importante! aca estamos usando una modalidad diferente a la documentacion oficial. estamos instalando el ejecutable de la version especifica de jekyll. no esta en usr/bin, sino que esta en el /bin del proyecto. es relativo al proyecto

nos va a tirar la lista de opciones de comandos para ejecutar

##  usamos build:
    
 ``` ./bin/jekyll build ```
    
##  vamo a ver el site

``` ls _site ```

##  vamo a correr localmente el sitio

``` ./bin/jekyll serve```

usamos serve para correr como servidor local
esta preparado para hacer cambios en vivo y no tener que darle build todo el rato
en la terminal voy a ver como corre, y si ponemos la direccion que nos tira, a traves del navegador web, voy a ver como anda el sitio 
es el localhost, la ip de mi maquina 127.0.0.1

 instalacion de gemas. 

 llegamos hasta aca. vamos a subir el sitio al gitlab y trabajarlo como tarea

# Subida del repo a gitlab

* vamos al grupo que creamos
* vamos a Nuevo Proyecto para crearlo; tomamos como criterio para nombrarlo el mismo que nuestro directorio (hlv.neocities.org)
* lo creamos publico y sin historia (no le ponemos readme)
* vemos opciones de clonado: ssh y hhtps
* !! vamos a usar ssh!  si no tenemos clave ssh creada y subida, podemos usar la documentacion de gitlab (la subimos despues, mientras emprolijamos la docu) 

* vemos todos los repos remotos

``` git remote -v ```


* agregamos el repositorio con git remote add [nuestro repo]

``` git remote add \0xacab\git@0xacab.org:taller-de-jekyll/hlv.neocities.org.git ```

* 
``` git remote -v ```

vamos a completar esto! pero ya lo pusheamos! esta subidoou!

##### comentarios que flotan
* darkhttpd es un programa servidor para correrlo localmente 
* como regla general, cuando estoy trabajando en un proyecto, me quedo en la carpeta raiz para trabajarlo

# Tarea: Eliminar los posts de prueba y crear propios, podemos basarnos en los archivos que ya están en la carpeta _posts
idealmente, los trabajamos localmente para poder practicar git
## No saben! nadie hace la tarea :p

# CLASE QUE VIENE:
    * EDITAMOS POSTS CON EL FORMATO MD
    * SUBIMOS EL SITIO A NEOCITIES
    
 # ENCUESTA DE HORARIOS:
     

         QUIEREN VENIR MAS TARDE? SE LES ESTA JUNTANDO CON EL ALMUERZO? +1


