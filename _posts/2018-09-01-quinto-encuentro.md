---
layout: post
title: Quinto Encuentro
---

## Git

git es un sistema, herramienta o cosa para trabajar colaborativamente proyectos comunes
internamente mantiene la historia de un proyecto, en forma de hitos en los que podemos ver los cambios. lo podemos visualizar como un arbol historico.

en la medida que se agregan modificaciones por parte de las distintas usuarias el proyecto se empieza a parecer a volver al futuro, con bifurcaciones, conflictos, marties desapareciendo.  esto se puede visualizar luego.

El make es otro  programa que es como una especie de seudolenguaje.  


## Registrar un sitio en neocities.org

Hay que crear un subdominio, con el nombre que queramos (y esté disponible) y una contraseña. Neocities tiene un plan gratuito con 1 GB de almacenamiento.

Se tiene que registrar "una persona" (para repensar los modelos de construccion colectiva). pero nos deja loguearnos a todas!

Usan IPFS: Inter Planetary File System!

Cuando entras te queda una cache que se puede usar como torrent. Lo buscamos para verlo y promover su uso (para que se potencie y tenga sentido), como una herramienta-estrategia anticensura! 




# Markdown

es una manera de darle formato a un texto, de forma simple, mas simple que escribir html.  es muy parecido a escribir cotidianamente y ademas deja las cosas listas para subir a un sitio.  los simbolos que usamos le dan sentido semantico, qué va a ser un titulo, una cita, un link.

Está bueno porque no es un lenguaje específico que vamos a usar solo para Jekyll, sino que se usa en un montón de plataformas, porque es fácil de recordar, facilita la lectura y además fortalece la seguridad de las plataformas que lo admiten, si permitieran que las usuarias subamos texto con formato en HTML, tendrían que cuidarse de que no les clavemos malware.

* [CommonMark](https://commonmark.org/) es el estándar que están coordinando distintos proyectos que implementan Markdown.  [Chuleta](https://commonmark.org/help/>)

* [Daring Fireball](https://daringfireball.net/projects/markdown/) es la primera implementación de Markdown básico, la dejamos por razones históricas, pero preferimos CommonMark porque ha evolucionado.

Pandoc es el intérprete de Markdown más completo y además permite convertir Markdown a unos 40 formatos, por ejemplo podemos escribir un artículo en Markdown y publicarlo luego como PDF o ePub, o subirlo a una wiki.  No lo vamos a usar todavía pero la mencionamos porque es una herramienta muy buena.


# tarea propuesta para la proxima: 

+ buscar editores con previsualizacion de md
+ unificar criterios de publicacion (o manual de estilo :p )
