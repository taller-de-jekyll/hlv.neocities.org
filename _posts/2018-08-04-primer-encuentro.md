---
layout: post
title: Primer encuentro
author: Taller de Jekyll
---

Taller De Jekyll 1
==================

* [Pad del taller, sin conexión a internet](http://192.168.1.121:9001/p/TallerDeJekyll1)
* [Pad del taller de Jekyll](https://pad.partidopirata.com.ar/p/TallerDeJekyll1)
* [Pad rescatado](https://etherpad.net/p/tallerdejekyll)
* [Pad cifrado con previsualización de Markdown](https://cryptpad.fr/code/#/2/code/edit/CPSzNdAEUC5NrkcpUEtffobX/)

### **Quiénes somos**

- Elio: el
- Lorna: elle/a
- Alan: el
- Cecilia: ella
- Libre: el?/le
- Alf: el-ella-ellas
- Monica: x/a
- Dani: ella
- Fauno: el-ellas
- Franco: da lo mismo
- Lucian: ella/elle
- Guten: el


Pads
----

Tienen colores! Salvo <https://cryptpad.fr>

Nos sirven para documentar el taller

Cuidados Colectivos Acordados: Usamos los códigos para compartir

### Intereses/ Expectativas

Aspectos relacionados con la seguridad, dinamizar el armado de sitios (desde los conocimientos que ya tenia )

### Ideas y propuestas

Sitio hacklab violeta con documentacion del curso.

## **Marco**

### Chistes de geocities.

* Te enchufaban publicidad random

* "Antes aprendías y ahorita está caralibro" (...) "Lo peor es la dependencia"

* "Nuestros valores con las maquinolas"

* Hardware: mambos de usabilidad. 

* "Te cambian las cosas de lugar o estan fijadas para un fin no esperado."

* Dejar de hablar de usuarix, en realidad la persona q interactua con la maquinola, (metáfora de persona cyborg) 

* Usuarix y maquinola son una continuidad? 

### Preguntas

**¿Qué son los sitios estáticos?**

    Respuesta: ver artículo en [gender it](https://www.genderit.org/node/5090) y [Usuaria turing completa](https://endefensadelsl.org/usuaria_turing_completa.html)

**¿Cómo se definen las usuarias en realación a la tecnología?**

    A partir de la web 2.0 hay una invisibilidad de la tecnología (sic).
    "Web 2.0 el capitalismo pone la fábrica y nosotras le ponemos el valor"
    Como el touch de los celulares es invisibilización de la tecnología o hablarle al celular.  El dispositivo decide qué hace con nuestro tacto.
    Todo es cada vez mas invisible para la usuaria, lo resolvemos por vos para que vos te dediques a lo que te parece importante.  (usuaria turing completa) esto invisibiliza el trabajo 
    que implica la construccion de estas tecnologias. Si podemos acceder al codigo, con sus niveles de complejidad y especializacion, podemos saber que programas estan corriendo y que 
    esten haciendo.  La apuesta es que esto sea cada vez mas invisible y complejo.
    Dejar de hablar de usuaria sugiere que la relación entre usuaria y maquina es meramente utilitaria, sin embargo termina siendo mas complejo donde la experiencia es total, pero a la vez, 
    viviendo en cuerpxs alienadxs, continuamos usando maquinas alienadas.
    Las interfaces nos alienan de la maquina, organizando el trabajo que hacemos con ella.
    web 2.0 como revolución industrial de la internet, donde pusieron los medios de producción y nos mostraron como anticuadas todas las demás herramientas que usábamos.
    En la web 1.0 teniamos la autonomia de aprender a hacer las cosas como queriamos, nos tocaba aprender, teniamos el frontpage! Y las herramientas de exportación de OpenOffice.
    La web 2.0 genera dependencia, cuando desaparece la plataforma perdes todo. 
  
**¿Cómo conectamos nuestros valores (politicos?) con los medios de produccion fisicos y digitales?**

    Identificamos que hay una presión en cómo se solucionan las cosas de forma hegemonica. Esas formas suelen restringir nuestra auonomía en el sentido que su dominio 
    nos requiere mucho esfuerzo para aprender. Esfuerzo técnico y político para descartar los usos anteriores (tablas).
    "No es profesional" y "No te lo posiciona google"

**¿Qué tiene wordpress?**

    Para mantener el sitio seguro tenés q estarle encima y estudiar q es vulnerable


**¿El hosting es gratarola o pago? ¿Hay una alternativa q no sea comercial?**

    Hosting com vs hosting gratuito


**Sitios seguros para causas feministas**


    Por las denuncias, ataques y baneo. Si te censuran en face y cagaste.


**Pensar integralmente la comunicación**


    ¿Qué plataforma necesitamos realmente? ¿Quien va a mantener los plugings y las actualizaciones?


**Las paginas estáticas.**


    Son archivos de texto, requieren menos actualización y son mas seguros. 
    Menos trabajo invisibilizado. Ecología (más que economía) de los recursos.
    Generadores de sitos estáticos.
    Colección de archivos, el software lo junta a todo. Haces una modificación y une todo nuevamente. 


**Alojamiento**

    Sitios en todos lados que no esten alojados en una sola computadora (estrategia de wikileaks y tpb)


**¿Podemos poner un sitio en un libro?**

**Arduino argento**

ipfs

    Archivos MD markdown
    
**¿Por qué un sitio estático?**

    Nos permite tener sitios autogestivos y seguros
    Tienen buen rendimiento
    Ocupan menos espacio
    Se pueden hacer compias descentralizadas.
    Los sitios estáticos igual no son la panacea.
    Esto nos lleva a la dependencia, porque los elementos en juego empiezan a ser cada vez mas complejos, con lo que se vuelve caro en tiempos propios para dominarlos y 
    empezamos a delegar en plataformas privativas. De otra forma hacerlo nosotras incluso basandonos en herramientas libres como wordpress, nos mete en un ciclo de estar 
    detras de las actualizaciones y vulnerabilidades.

**¿Por que queremos que nuestros sitios sean seguros para causas feministas?**

    Los motivos fundamentales son que las causas feministas son trolleadas, denunciadas, cerradas, agredidas.  Facebook nos cierra la página y perdemos todo. 
    Queremos tener sitios autogestivos. Nos atacan pero lo podemos hacer de vuelta, pero si dependemos de plataformas como facebook nos censuran efectivamente 
    porque la informacion se pierde. Son como los fanzines o el samizdat (auto publicación)  en el ciberespacio. 
    ¿Quién va a tener el tiempo de mantener todo eso? Plugins y spam en WordPress, por ejemplo.

[Leemos nota de genderit sobre autonomia tecnologica y feminismos.](https://www.genderit.org/node/5090)

**¿Qué son los sitios estáticos?**

    El sitio se genera una sola vez y asi nos ahorramos la base de datos, recursos del servidor por cada visita que se  no hace falta estar corriendo un software. 
    La estrategia sería evitar tener que dedicarle muchos recursos al sitio, evitar invertir esfuerzos y dineros para alojarnos y poder utilizar servidores domesticos para eso. 
    Además requiere mucha menos expertise y tiene asociado menos trabajo invisibilizado. 
    -Sitios transportables 
    -Sitios estaticos--->> lograr independencia/menos vulnerables
    -Sitios dinámicos: se generan cada vez que accedemos a ellos, hay un programa que tiene que estar corriendo siempre para que se pueda generar el sitio cada vez 
    -> dependemos de la infraestructura, de bases de datos, de una cantidad de recursos grande, etc.
    Generadores de sitios estaticos-----> jekyll/ pelikan (pyton) Cada lenguaje de programacion tiene al menos uno que genera sitios estaticos
    Los sitios estáticos usan archivos que tienen distintos roles: algunos archivos de contenido, algunos de diseño y otros de estructura. Una vez que tenemos todos 
    los archivos en una carpeta podemos compartirla por los medios que querramos.


**¿Cómo instalar los paquetes en sus gnu/linux: git, make, build-essential (debian, ubuntu, mint y derivadas), base-devel (archlinux, parabola, manjaro, etc)?**

**Con Mint**

    Menú, administración, gestor de paquetes Synaptic, pide autenticación.
    Dentro del gestor click en Buscar, abre una ventanita, se pone el nombre del paquete y que busque por Nombre. 
    Una vez que aparece en el listado el paquete, click con botón derecho, Marcar para instalar.
    Te dice qque va a agregar otros paquetes asociados, marca otros items.
    Y una vez marcado vas al botón Aplicar, y te abre una ventana que pregunta:
    ¿Quiere aplicar los cambios siguientes? y te da un detalle de cuales son los paquetes que va a instalar y cuales va a cambiar o no...
    Aplicar, y empieza a descargar los paquetes y, si todo sale bien, al final te dice que está todo bien, o... puede fallar.

***
