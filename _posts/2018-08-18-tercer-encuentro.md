# TERCER ENCUENTRO

No tuvimos luz, así que vamos a repasar algunas cosas y recuperar luego.

* Abrimos cuentas en [0xacab](https://0xacab.org), una plataforma gestionada por <https://riseup.net/> para alojar repositorios git, basada en el software libre [Gitlab](https://gitlab.org/).

* Programa para visualizar git : 
    
tig:

* en fedora sudo dnf install tig

* en ubuntudebian sudo apt-get install tig

* o https://jonas.github.io/tig/INSTALL.html 

     

* Debatimos sobre git

## Diferentes  esquemas: 
    
* Modelo centralizado: 1 repositorio donde todas forkeamos el proyecto lo trabajamos de forma local y luego necesitamos que aprueben los cambios
* Modelo distribuido: 1 repositorio donde  clonamos y luego y todas nos traemos los cambios y podemos editar sin restricción 
    
    
    
> Markdown se escribe en texto plano --> Jekyll lo pasa a html para visualizar 
    



## Markdown (.md)
Markdown es un lenguaje de marcado. Tiene una serie de reglas de sintaxis:
    https://markdown.es/sintaxis-markdown/ 