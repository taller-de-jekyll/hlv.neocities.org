---
layout: post
title: Sexto Encuentro
---


# Sexto encuentro (8 de septiembre)

SSH es un protocolo para traer y enviar datos que nos permite crear llaves
públicas  que nos sirve para que no tengamos que tipear nuestra contraseña.

Desde el tiempo de nuestros antepasados la forma de enviar un mensaje
ocultándolo a intermediarias es cifrándolo con un secreto conocido (una
contraseña y/o un protocolo de  cifrado/ofuscación como rot13 el que usaban
los milicos romanos).  El problema es que ese  secreto hay que compartirlo de
otra forma, no puede ir acompañando el mensaje, sino  cualquier intercepción
permite decifrar el mensaje y todo el cifrado pierde sentido.

Entonces hay que mandarlos por caminos separados o acordarlos de antemano y
todo  es un quilombo.

Esto se llama cifrado simétrico. Todas las partes saben cómo decifrar el
mensaje y la  información requerida para cifrar es la misma para decifrar.
Con el cifrado asimétrico  la información que cifra no es la misma que la que
decifra, lo que introduce la ventaja que podemos repartir abiertamente el
material que cifra, la llave pública, como si fueran  candados abiertos.  Ese
candado puede ser cerrado por cualquiera que nos quiera mandar un mensaje pero
solo puede ser abierto por una llave que no se distribuye y que solo tenemos
nosotras, es decir la llave privada.

Así se evita tener que pre-acordar contraseñas y la distribución y publicación
de una llave pública (candado abierto) no implica mayores problemas.  Las
llaves privadas viven en nuestras computadodoras y para obtenerlas y decifrar
los mensajes que recibimos tienen que atacarlas físicamente, sea robándolas o
instalando malware.

Además las llaves privadas se pueden cifrar simétricamente con una contraseña,
de forma que volvemos a separar la información, por un lado la información
"física" de decifrado y por otro la información "intangible" que solo nosotras
sabemos (¡y no nos podemos olvidar!) a menos que nos las saquen a manguerazos.
En criptojerga, algo que sabes y algo que tenés (como el pin de la tarjeta de
débito y la tarjeta de débito, no como la de crédito que tiene el pin
escrito).

El cifrado asimétrico se usa en todos lados, GPG, SSH, HTTPS, etc. aunque en
HTTPS es más  complicado porque no tenemos todas las llaves públicas de todos
los sitios, sino que hay  entidades llamadas Certificate Authority (CA,
Autoridad Certificante) que notarían otros  certificados.  Los navegadores
solo tienen las llaves públicas de las CA y con eso verifican  que las llaves
públicas de cada sitio sean válidas.  Esto genera un oligopolio de quien puede
validar un sitio y cuánto sale.  También está Let's Encrypt, un proyecto de la
EFF para emitir  certificados gratuitos, pero que no modifica la estructura
jerárquica/oligopólica de CAs, sino  que agrega una buena onda.  Igual la
queremos!

## Crear llave SSH

Para crear una llave SSH abrimos la terminal e ingresamos ssh-keygen, nos va a
pedir que le  asignemos un directorio y un nombre a los archivos de las llaves
que se van a generar (por  defecto nos sugiere una ruta), lo más recomendable
es aceptar (enter) a todo lo que sigue.  Ahí nos pide que generemos una
contraseña para nuestra llave privada (podemos elegir no  ponerle contraseña a
nuestra llave privada). Entonces ya tenemos creada nuestras llaves  pública y
privada. A continuación vamos a 0xacab, entramos en "agregar llaves ssh". En
la  carpeta .ssh que generamos con el comando ssh-keygen van a haber dos
archivos, la llave  pública es la que termina con la extensión .pub . Si
abrimos ese archivo vemos nuestra llave  pública, la tenemos que copiar y
pegar en el campo "key" de 0xacab. Le damos a "Add key" o  "Agregar llave" y
listo.

## Clonar el repo usandola llave SSH que creamos

Para copiarnos (clonar) el repo remoto de nuestro repo a nuestro disco local
escribimos en  la terminal: git clone "la dirección ssh del repo". Ahí nos va
a pedir la contraseña de  nuestra llave ssh privada (lo mismo va a pasar cada
vez que querramos pullear/ pushear) Una vez que tengamos el repo clonado,
abrimos la terminal y hacemos: bundle install --path=~/. gem Bundler es un
gestor de gemas que permite instalar los paquetes acordes a las versiones de
las gemas con las que trabajamos en cada proyecto.

## Clave SSH en Oxacab

Desde https://0xacab.org/profile  A la izquierda hay un panel vertical que
dice SSH Keys Ahí se puede subir y también linkear a la ayuda para crearla.
Puede subirse más de una, asociar diferentes dispositivos, etc.


## Las gemas y el Gemfile

## Bundle y binstubs

## Neocities

Neocities es un sitio que aloja webs de forma gratuita, para subir el sitio
utilizamos una  gema con la cual haremos un push de nuesto sitio

1. instalar la gema de neocities: editamos el archivo  tale.gemspec y agregamos la siguiente 
linea:

```spec.add_development_dependency "neocities"```

esto sumará la gema neocities para que bundle la encuentre y la sume a nuestro proyecto

2. tener las claves del servicio de neocities

3. ejecutar gema neocities estando en el directorio del sitio: `./bin/neocities`

4. para subir el sitio: `neocities push ./_site`

## Tarea

Revisar la [página](hlv.neocities.org) para detectar lo que se tenga que arreglar, o 
modificar para que quede más linda.

1. Los links a los post no funcionan. Aparentemente no se subieron los archivos.
2. Se podría agregar un encabezado, con título y datos para saber de qué se trata la página.
3. También se podría agregar un favicon. // cambie el favicon.ico por otra imagen, desde un 
png hecho en inkscape lo pase por esta web https://convertico.com/ y me devolvió el .ico --
librenau
4. Los post están desordenados. Se tienen que ordenar por... para que queden de forma 
cronológica. //ya están por fecha --librenau
5. Podrá ser alguna imagen alegorica como fondo del encabezado, no?
6. Un pié con datos de contacto quienes somos y eso?
7. escribir texto de acerca
8. cambiar hlv por "hlv ~ hacklab violeta"
9. terce9r encuentro le falta formato
10. borrar los articulos por defecto
11. las fechas estan en ingles
12. color estridente de fondo (fucsia)
13. cargar la convocatoria para el 29
14. cargar memorias del 7, 8 y 9 encuentros
15. cambiar lx autorx de los posts